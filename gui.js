/* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--                       Yatzy - DIP-project                         --
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--       Copyright 2018 Flemming Pedersen & Christian Petersen       --
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- */

function init() {
    gui = new GUI()
    gui.setup()
}

/**
 * @constructor
 * @this: {GUI} 
 */
function GUI() {
    this.game = new Game()
    /**
     * Runs all functions required to setup the games graphical representation.
     */
    this.setup = function () {
        this.createScoreBoard(this.game.getScoreLines())
        let roll_button = document.getElementById("diceRoller")
        roll_button.gui = this
        roll_button.addEventListener('click', this.guiRoll)
        this.dieEventAdder()
    }

    this.roundCounter = 0;

    /**
     * When run, disables the buttons used for scoring.
     */
    this.disableScoring = function () {
        let scoreButtons = document.getElementsByClassName("scoreSelect")
        for (b of scoreButtons) {
            if (!b.disabled) {
                b.disabled = true
            }
        }
        this.roundCounter++
        if (this.roundCounter >= 15) {
            this.gameEnd()
        }
    }

    /**
     * When run, enables the buttons used for scoring.
     */
    this.enableScoring = function () {
        let scoreButtons = document.getElementsByClassName("scoreSelect")
        for (b of scoreButtons) {
            if (!b.getAttribute("locked")) {
                b.disabled = false
            }
        }
    }

    /**
     * Resets the dice locks, so that no dice are locked, and all are ready to be rolled.
     * @param {Round} round 
     */
    this.resetDiceLocks = function (round) {
        let dice = document.getElementsByClassName("diceBox")
        for (d of dice) {
            d.setAttribute("class", "diceBox")
        }
        for (d of round.dice) {
            d.locked = false
        }
    }

    this.updateDice = function (round) {
        let dice = document.getElementsByClassName("diceBox")
        for (let i = dice.length; i--;) {
            dice[i].setAttribute('value', round.dice[i].value)
        }
    }


    this.guiRoll = function () {
        let gui = this.gui
        let game = gui.game
        let currentRolled = game.round.roll()
        if (currentRolled >= 3) {
            document.getElementById("diceRoller").disabled = true
            gui.enableScoring()
            gui.resetDiceLocks(game.round)
        }
        gui.updateDice(game.round)
        gui.updateScoreboard(game.getScoreLines(), game.round)
    }

    /**
     * @this: {DOM.Button}
     */
    this.lockScore = function () {
        let game = this.gui.game
        let scores = game.getScoreLines()
        let scoreLine = scores[this.lineName]
        scoreLine.amount = scoreLine.checkPotential(game.round.gatherEyes())
        let line = document.getElementById(this.lineName)
        line.setAttribute("scored", true);
        let rollButton = document.getElementById("diceRoller")
        rollButton.disabled = false
        game.round.newRound()
        let button = line.getElementsByClassName("scoreSelect")[0]
        button.disabled = true
        button.setAttribute("locked", "true")
        this.gui.disableScoring()
    }

    /**
     * Creates the html elements for the scoreboard, and binds its buttons to correct events.
     * @param {scoreLines} scoreBoard 
     */
    this.createScoreBoard = function (scoreBoard) {
        let scoreboardDiv = document.getElementById('scoreboard')
        for (line_name in scoreBoard) {
            let line = scoreBoard[line_name]
            let scoreLine = document.createElement("div")
            scoreLine.setAttribute("class", "boardRow")
            scoreLine.setAttribute("id", line_name)
            let name = document.createElement("div")
            name.setAttribute("class", "boardCell")
            name.innerHTML = line.name
            let score = document.createElement("div")
            score.setAttribute("class", "boardCell score")
            score.setAttribute("id", line_name)
            let scoreValue = document.createElement("p")
            scoreValue.innerHTML = line.amount
            score.appendChild(scoreValue)
            let button = document.createElement("div")
            button.setAttribute("class", "boardCell")
            let innerButton = document.createElement("button")
            innerButton.setAttribute("class", "scoreSelect")
            innerButton.onclick = this.lockScore
            innerButton.lineName = line_name
            innerButton.gui = this
            innerButton.disabled = true
            button.appendChild(innerButton)
            scoreLine.appendChild(name)
            scoreLine.appendChild(button)
            scoreLine.appendChild(score)
            scoreboardDiv.appendChild(scoreLine)
        }
    }

    this.updateScoreboard = function (scores, round) {
        for (line_name in scores) {
            let line = document.getElementById(line_name)
            if (line != undefined) {
                let scored = line.getAttribute("scored")
                if (!scored) {
                    let line_value = document.getElementById(line_name).getElementsByTagName('p')[0]
                    line_value.innerHTML = scores[line_name].checkPotential(round.gatherEyes())
                }
            }
        }
    }

    this.dieEventAdder = function () {
        let dieButtons = document.getElementsByClassName('diceBox')
        let i = 0
        for (die of dieButtons) {
            die.gui = this
            die.addEventListener("click", gui.holdDie, false)
            i++
        }
    }

    /**
     * 
     * @param {eventCaller} evt
     * @this: {DOM.Button}
     */
    this.holdDie = function (evt) {
        let gui = this.gui
        let round = gui.game.round
        let i = evt.toElement.attributes.index.value
        let dieButtons = document.getElementsByClassName('diceBox')
        if (round.dice[i].locked) {
            round.dice[i].locked = false
            dieButtons[i].setAttribute("class", "diceBox");
        } else {
            round.dice[i].locked = true
            dieButtons[i].setAttribute("class", "diceBox locked");
        }
    }

    this.gameEnd = function () {
        let score = this.game.calcTotal()
        let sum = score[0]
        let upper = score[1]
        let bonus = score[2]
        let lower = sum - upper - bonus
        openModal(upper, bonus, lower, sum)
    }
}