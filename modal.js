/* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--                       Yatzy - DIP-project                         --
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--       Copyright 2018 Flemming Pedersen & Christian Petersen       --
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- */

let modal = document.getElementById('scoreModal')

let span = document.getElementsByClassName("close")[0]

span.onclick = function () {
    modal.style.display = "none"
    location.reload()
}

window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none"
        location.reload()
    }
}

let max = 374

let modalContent = document.getElementById('modalContent')

openModal = function (upper, bonus, lower, total) {
    let upperP = document.getElementById('upperScore')
    let bonusP = document.getElementById('bonusScore')
    let lowerP = document.getElementById('lowerScore')
    let totalP = document.getElementById('totalScore')
    if (bonus != 50) {
        modalContent.style.fontFamily = '"Comic Sans MS", cursive, sans-serif'
    }
    let diff = max - total;
    let red = diff / max * 230
    let green = diff / max * 230
    let blue = diff / max * 250
    modalContent.style.borderColor = "rgb(" + red + "," + green + "," + blue + ")"
    upperP.innerHTML = "Upper Score: " + upper
    bonusP.innerHTML = "Bonus Score: " + bonus
    lowerP.innerHTML = "Lower Score: " + lower
    totalP.innerHTML = "Total Score: " + total
    modal.style.display = "block"
}

374 - 316