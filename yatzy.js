/* -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--                       Yatzy - DIP-project                         --
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
--       Copyright 2018 Flemming Pedersen & Christian Petersen       --
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- */


/**
 * @constructor
 * 
 * Simple die object with internal roll function.
 */
function Die() {
  this.value = 0;
  this.locked = false;
  this.diceRoll = function () {
    let rnd = Math.random();
    this.value = (Math.floor(rnd * 6)) + 1
  }
}

/**
 * @constructor
 * Created only inside Game-object.
 */
function Round() {
  this.dice = new Array(new Die(), new Die(), new Die(), new Die(), new Die());
  this.current_rolled = 0;

  this.newRound = function () {
    this.current_rolled = 0
  }

  this.roll = function () {
    for (d of this.dice) {
      if (!d.locked)
        d.diceRoll()
    }
    this.current_rolled++
    return this.current_rolled
  }

  this.gatherEyes = function () {
    let diceFaces = [0, 0, 0, 0, 0, 0]

    for (die of this.dice) {
      diceFaces[die.value - 1]++
    }
    return diceFaces
  }
}

/**
 * @constructor
 * Creates new game with new scoreboard.
 */
function Game() {
  this.round = new Round()
  this.scores = new scoreLines()

  this.getScoreLines = function () {
    return this.scores
  }

  this.calcTotal = function () {
    let sum = 0
    let upperSection_sum = 0
    let bonus = 0
    let scoreArray = Object.values(this.scores)
    for (i = scoreArray.length; i--;) {
      sum += scoreArray[i].amount
      if (i < 6) {
        upperSection_sum += scoreArray[i].amount
      }
    }
    if (upperSection_sum >= 63) {
      sum += 50
      bonus = 50
    }
    let finalScore = [sum, upperSection_sum, bonus]
    return finalScore

  }

  /**
   * @constructor
   * 
   * Creates table containing categories, their saved amount, if category is already chosen
   * and checkPotential(); returning potential score.
   * 
   */
  function scoreLines() {
    this.ones_Score = {
        locked: false,
        amount: 0,
        name: "Ones",
        checkPotential: function (diceFaces) {
          if (diceFaces[0] > 0) {
            return diceFaces[0]
          }
          return 0
        },
      },
      this.twos_Score = {
        locked: false,
        amount: 0,
        name: "Twoes",
        checkPotential: function (diceFaces) {
          if (diceFaces[1] > 0) {
            return diceFaces[1] * 2
          }
          return 0
        },
      },
      this.threes_Score = {
        locked: false,
        amount: 0,
        name: "Threes",
        checkPotential: function (diceFaces) {
          if (diceFaces[2] > 0) {
            return diceFaces[2] * 3
          }
          return 0
        },
      },
      this.fours_Score = {
        locked: false,
        amount: 0,
        name: "Fours",
        checkPotential: function (diceFaces) {
          if (diceFaces[3] > 0) {
            return diceFaces[3] * 4
          }
          return 0
        },
      },
      this.fives_Score = {
        locked: false,
        amount: 0,
        name: "Fives",
        checkPotential: function (diceFaces) {
          if (diceFaces[4] > 0) {
            return diceFaces[4] * 5
          }
          return 0
        },
      },
      this.sixes_Score = {
        locked: false,
        amount: 0,
        name: "Sixes",
        checkPotential: function (diceFaces) {
          if (diceFaces[5] > 0) {
            return diceFaces[5] * 6
          }
          return 0
        },
      },
      this.onePair_Score = {
        locked: false,
        amount: 0,
        name: "One Pair",
        checkPotential: function (diceFaces) {
          for (let i = diceFaces.length; i--;) {
            if (diceFaces[i] >= 2) {
              return 2 * (i + 1)
            }
          }
          return 0
        },
      },
      this.twoPair_Score = {
        locked: false,
        amount: 0,
        name: "Two Pairs",
        checkPotential: function (diceFaces) {
          let pairCount = 0
          let pairAmount = 0
          for (d in diceFaces) {
            if (diceFaces[d] >= 2) {
              pairCount++
              pairAmount += 2 * (parseInt(d) + 1)
            }
          }
          if (pairCount >= 2) {
            return pairAmount
          } else {
            return 0
          }

        },
      },
      this.threeSame_Score = {
        locked: false,
        amount: 0,
        name: "Three same",
        checkPotential: function (diceFaces) {
          for (d in diceFaces) {
            if (diceFaces[d] >= 3) {
              return (parseInt(d) + 1) * 3
            }
          }
          return 0
        },
      },
      this.fourSame_Score = {
        locked: false,
        amount: 0,
        name: "Four same",
        checkPotential: function (diceFaces) {
          for (d in diceFaces) {
            if (diceFaces[d] >= 4) {
              return (parseInt(d) + 1) * 4
            }
          }
          return 0
        },
      },
      this.smallStraight_Score = {
        locked: false,
        amount: 0,
        name: "Small Straight",
        checkPotential: function (diceFaces) {
          if (diceFaces[0] >= 1 && diceFaces[1] >= 1 && diceFaces[2] >= 1 && diceFaces[3] >= 1 && diceFaces[4] >= 1) {
            return 15
          }
          return 0
        },
      },
      this.bigStraight_Score = {
        locked: false,
        amount: 0,
        name: "Big Straight",
        checkPotential: function (diceFaces) {
          if (diceFaces[1] >= 1 && diceFaces[2] >= 1 && diceFaces[3] >= 1 && diceFaces[4] >= 1 && diceFaces[5] >= 1) {
            return 20
          }
          return 0
        },
      },
      this.fullHouse_Score = {
        locked: false,
        amount: 0,
        name: "Full House",
        checkPotential: function (diceFaces) {
          let threeSame_count = 0
          let twoSame_count = 0
          let houseAmount = 0
          for (let i = diceFaces.length; i--;) {
            if (diceFaces[i] == 3) {
              threeSame_count++
              houseAmount += 3 * (i + 1)
            }
            if (diceFaces[i] == 2) {
              twoSame_count++
              houseAmount += 2 * (i + 1)
            }
          }
          if (threeSame_count > 0 && twoSame_count > 0) {
            return houseAmount
          } else {
            return 0
          }
        },
      },
      this.chance_Score = {
        locked: false,
        amount: 0,
        name: "Chance",
        checkPotential: function (diceFaces) {
          let chance_Sum = 0
          for (let i = diceFaces.length; i--;) {
            chance_Sum += (i + 1) * diceFaces[i]
          }
          return chance_Sum
        }
      },
      this.yatzy_Score = {
        locked: false,
        amount: 0,
        name: "Yatzy",
        checkPotential: function (diceFaces) {
          for (d of diceFaces) {
            if (d >= 5) {
              return 50
            }
          }
          return 0
        },
      }
  }
}